from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Edward Partogi Gembira Abyatar' # TODO Implement this
kanan_name = 'Azhardifa Arnanda'
kiri_name = 'Gde Indra'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 9, 29) #TODO Implement this, format (Year, Month, Date)
kanan_birth_date = date(1999, 9, 22)
kiri_birth_date = date(1999, 2, 2)
npm = 1706979215 # TODO Implement this
kanan_npm = 1706039540
kiri_npm = 17069844606
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'name2': kanan_name, 'name3': kiri_name, 'age2': calculate_age(kanan_birth_date.year), 'age3': calculate_age(kiri_birth_date.year), 'age': calculate_age(birth_date.year), 'npm': npm, 'npm2': kanan_npm, 'npm3': kiri_npm}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
